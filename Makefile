# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cboyer <cboyer@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/01/08 12:28:01 by cboyer            #+#    #+#              #
#    Updated: 2018/03/12 17:02:11 by cboyer           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRC =	main.c					\
		malloc.c				\
		ft_itoa_base.c			\
		malloc_utils.c			\
		free_utils.c			\
		count_size.c			\
		free.c					\
		realloc.c				\
		show_alloc_mem.c		\


SRC_PATH = $(addprefix ./src/, $(SRC))
OBJ = $(SRC_PATH:.c=.o)
NAME = malloc
CC = gcc
RM = rm -f
CFLAGS = -g -Wall -Werror -Wextra

LIB_PATH = ./libft/
LIB_NAME = -lft
LIB = $(addprefix -L,$(LIB_PATH))
LFLAGS = $(LIB) $(LIB_NAME)

INC_PATH = ./libft/includes/ ./inc/
INC = $(addprefix -I,$(INC_PATH))

all: lib $(NAME)

lib:
	make -C ./libft

$(NAME): $(OBJ)
	$(CC) $(CFLAGS) $(LFLAGS) $(OBJ) -o $@

%.o: %.c
	$(CC) $(INC) -o $@ -c $^ $(CFLAGS)

clean:
	$(RM) $(OBJ)
	make clean -C ./libft

fclean: clean
	$(RM) $(NAME)
	make fclean -C ./libft

re: fclean all
