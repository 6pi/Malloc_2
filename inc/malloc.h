/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cboyer <cboyer@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/28 15:03:13 by cboyer            #+#    #+#             */
/*   Updated: 2018/03/12 17:04:51 by cboyer           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_H
# define MALLOC_H
# include <stdlib.h>
# include <sys/mman.h>
# include <unistd.h>
# include <string.h>
# include "libft.h"
# include <time.h>
#include <stdio.h>

# define PAGE_SIZE getpagesize()
# define TINY PAGE_SIZE
# define SMALL PAGE_SIZE * 20
# define TINY_PLAGE TINY * 100
# define SMALL_PLAGE SMALL * 50

# define FLAG_PROT PROT_READ | PROT_WRITE
# define FLAG_MAP MAP_ANONYMOUS | MAP_PRIVATE

typedef struct	s_plage
{
	int			tab[100];
	int			type;
	void		*ptr;
	void		*prev;
	void		*next;
}				t_plage;

typedef struct	s_large
{
	int			size;
	void		*ptr;
	void		*prev;
	void		*next;
}				t_large;

typedef struct	s_zone
{
	t_plage		*tiny;
	t_plage		*small;
	t_large		*large;
}				t_zone;

t_zone	zones;

void	ft_free(void *ptr);
void	*ft_malloc(size_t size);
void	ft_itoa_base(int n, int base);
void	create_plage(t_plage *plage, int type);
void	init_tab(int *tab, int size, int value);
void	check_del_plage(t_plage *plage);
int		get_limit(size_t size);
int		count_size(t_plage *zone);
int		count_size_large(t_large *plage);
t_plage	*find_plage(void *ptr);
t_large	*find_large(void *ptr);
void	unmap_plage_large(t_large *plage);
void	*ft_realloc(void *ptr, size_t size);
void	show_alloc_mem();

#endif
