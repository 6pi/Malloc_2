/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoinfree.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cboyer <cboyer@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/27 16:16:26 by cboyer            #+#    #+#             */
/*   Updated: 2017/04/27 16:16:28 by cboyer           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char    *ft_strjoinfree(char const *s1, char const *s2)
{
    char    *tmp;

    if (s1 == NULL || s2 == NULL)
        return (NULL);
    if ((tmp = malloc(sizeof(char) * (ft_strlen(s1) + ft_strlen(s2)) + 1))
                == NULL)
        return (NULL);
    ft_strcpy(tmp, s1);
    ft_strcat(tmp, s2);
    free((char*)s1);
    free((char*)s2);
    return (tmp);
}
