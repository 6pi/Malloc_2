/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   count_size.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cboyer <cboyer@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/16 12:06:25 by cboyer            #+#    #+#             */
/*   Updated: 2018/02/16 13:35:18 by cboyer           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

int		count_tab(t_plage *plage)
{
	int	i;
	int	tot;

	tot = 0;
	i = plage->type == 2 ? 50 : 100;
	while (i > 0)
	{
		tot += plage->tab[i];
		i--;
	}
	return (tot);
}

int		count_size_large(t_large *plage)
{
	int	tot;

	tot = 0;
	while (plage)
	{
		tot += plage->size;
		plage = plage->next;
	}
	return (tot);
}

int		count_size(t_plage *zone)
{
	t_plage	*plage;
	int		tot;

	tot = 0;
	plage = zone;
	while (plage)
	{
		tot += count_tab(zone);
		plage = plage->next;
	}
	return (tot);
}
