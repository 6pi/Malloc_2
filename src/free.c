/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cboyer <cboyer@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/11 12:26:20 by cboyer            #+#    #+#             */
/*   Updated: 2018/03/12 18:13:23 by cboyer           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void	unmap_plage_large(t_large *plage)
{
	if (plage->prev)
	{
		((t_large*)plage->next)->prev = plage->prev;
		((t_large*)plage->prev)->next = plage->next;
		munmap(plage, plage->size + sizeof(t_plage));
	}
	else if (plage->next)
	{
		zones.large = plage->next;
		((t_large*)plage->next)->prev = 0x0;
		munmap(plage, plage->size + sizeof(t_plage));
	}
	else
	{
		zones.large = 0x0;
		munmap(plage, plage->size + sizeof(t_plage));
	}
}

void	unmap_plage_st(t_plage *plage, int type)
{
	int		size;

	if (type == 2)
		size = SMALL_PLAGE + sizeof(t_plage);
	else
		size = TINY_PLAGE + sizeof(t_plage);
	if (plage->prev)
	{
		((t_plage*)plage->prev)->next = plage->next;
		munmap(plage, size);
	}
	else if (plage->next)
	{
		if (type == 2)
			zones.small = plage->next;
		else
			zones.tiny = plage->next;
		munmap(plage, size);
	}
	else
	{
		if (type == 2)
			zones.small = 0x0;
		else
			zones.tiny = 0x0;
		munmap(plage, size);
	}
}

t_large	*find_large(void *ptr)
{
	t_large	*large;

	large = zones.large;
	while (large)
	{
		if (large->ptr == ptr)
			return (large);
		large = large->next;
		printf("lal\n");
	}
	printf("none_l\n");
	return (0x0);
}

t_plage	*find_plage(void *ptr)
{
	t_plage	*plage;

	plage = zones.tiny;
	printf("%p \n", ptr);
	while (plage)
	{
		printf("plage->ptr: %p\n", plage->ptr);
		printf("plage->ptr + TINY: %p\n", plage->ptr + TINY_PLAGE);
		if (plage->ptr <= ptr && plage->ptr + TINY_PLAGE > ptr)
			return (plage);
		plage = plage->next;
	}
	plage = zones.small;
	while (plage)
	{
		printf("plage->ptr: %p\n", plage->ptr);
		printf("plage->ptr + SMALL: %p\n", plage->ptr + TINY_PLAGE);
		if (plage->ptr <= ptr && plage->ptr + SMALL_PLAGE > ptr)
			return (plage);
		plage = plage->next;
	}
	printf("none\n");
	return (0x0);
}

void	ft_free(void* ptr)
{
	t_plage	*plage;
	t_large	*large;
	int		i;

	plage = find_plage(ptr);
	large = find_large(ptr);
	if (large != 0x0)
	{
		unmap_plage_large(large);
		return ;
	}
	else if (plage != 0x0 && plage->type == 2)
	{
		printf("adresse PDV plage: %d\n", (int)ptr - (int)plage->ptr);
		i = ((int)ptr - (int)plage->ptr) / (int)SMALL;
		printf("i: %d\n", i);
		plage->tab[i] = -1;
		check_del_plage(plage);
	}
	else if (plage != 0x0 && plage->type == 1)
	{
		printf("adresse PDV plage: %d\n", (int)ptr - (int)plage->ptr);
		i = ((int)ptr - (int)plage->ptr) / (int)TINY;
		plage->tab[i] = -1;
		check_del_plage(plage);
	}
	else
		return ;
}
