/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cboyer <cboyer@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/11 14:47:36 by cboyer            #+#    #+#             */
/*   Updated: 2018/03/12 18:07:58 by cboyer           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void	check_del_plage(t_plage *plage)
{
	int		i;
	int		max;

	i = 0;
	max = plage->type == 2 ? 50 : 100;
	while (i < max)
	{
		if (plage->tab[i] > 0)
			return ;
		i++;
	}
	if (plage->prev)
		((t_plage*)(plage->prev))->next = plage->next;
	else if (plage->next && !plage->prev)
		zones.tiny = plage->next;
	else if (plage->prev && !plage->next)
		((t_plage*)(plage->prev))->next = 0x0;
	munmap(plage, plage->type == 2 ? SMALL_PLAGE + sizeof(t_plage) : TINY_PLAGE + sizeof(t_plage));
}
