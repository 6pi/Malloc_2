/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cboyer <cboyer@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/17 12:30:17 by cboyer            #+#    #+#             */
/*   Updated: 2018/03/12 17:54:29 by cboyer           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static int	ft_intlen_base(int n, int base)
{
	int i;

	i = 1;
	if (n < 0 && base == 10)
		i++;
	while (n > (base - 1) || (n < -9 && base == 10))
	{
		i++;
		n = n / base;
	}
	return (i);
}

void		ft_itoa_base(int n, int base)
{
	int		ntmp;
	int		len;
	int		i;
	char	tab[15];

	ft_bzero(tab, 15);
	len = ft_intlen_base(n, base);
	if (n < 0 && base == 10)
		ft_putstr("-");
	i = len - 1;
	while (n != 0)
	{
		ntmp = n > 0 ? (n % base) : -(n % base);
		//ft_putchar((char)ntmp > 9 ? ((ntmp - 10) + 'A') : (ntmp + '0'));
		tab[i] = (char)ntmp > 9 ? ((ntmp - 10) + 'A') : (ntmp + '0');
		i--;
		n = n / base;
	}
	ft_putstr(tab);
}
