/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cboyer <cboyer@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/11 15:08:57 by cboyer            #+#    #+#             */
/*   Updated: 2018/03/12 17:59:37 by cboyer           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

int main()
{
	char *ti = (char*)ft_malloc(10);
	char *ti1 = (char*)ft_malloc(10);
	char *ti2 = (char*)ft_malloc(10);
	char *sa = (char*)ft_malloc(TINY + 10);
	char *sa1 = (char*)ft_malloc(TINY + 10);
	char *sa2 = (char*)ft_malloc(TINY + 10);
	char *sa3 = (char*)ft_malloc(TINY + 10);
	char *la = (char*)ft_malloc(SMALL + 10);
	char *la1 = (char*)ft_malloc(SMALL + 10);
	char *la2 = (char*)ft_malloc(SMALL + 10);
	char *la3 = (char*)ft_malloc(SMALL + 10);
	int i = 0;

	while (i < 150)
	{
		if (i < 10)
		{
			ti[i] = 'a';
			ti1[i] = 'b';
			ti2[i] = 'c';
		}
		if (i < TINY + 9)
		{
			sa[i] = 'A';
			sa1[i] = 'B';
			sa2[i] = 'C';
			sa3[i] = 'D';
		}
		la[i] = '1';
		la1[i] = '2';
		la2[i] = '3';
		la3[i] = '4';
		i += 1;

	}

	printf("------------- MALLOC ----------\n\n");
	printf("%s\n", ti);
	printf("%s\n", ti1);
	printf("%s\n", ti2);
	printf("%s\n", sa);
	printf("%s\n", sa1);
	printf("%s\n", sa2);
	printf("%s\n", sa3);
	printf("%s\n", la);
	printf("%s\n", la1);
	printf("%s\n", la2);
	printf("%s\n", la3);

	show_alloc_mem();

	printf("----------- REALLOC --------------\n\n");

	ti = ft_realloc(ti, TINY + 10);
	ft_memcpy(ti, sa1, TINY );
	printf("new add: %p\n", ti);
	printf("TINY->SMALL\nti : \n%s\n", ti);
	printf("expected: \n%s\n", sa1);
	sa = ft_realloc(sa, SMALL + 10);
	ft_memcpy(sa, la1, SMALL + 10);
	printf("new add: %p\n", sa);
	printf("SMALL->LARGE sa: \n%s\n", sa);
	printf("expected: \n%s\n", la1);
	la = ft_realloc(la, 10);
	ft_memcpy(la, ti1, 10);
	printf("new add: %p\n", la);
	printf("LARGE->TINY: \n%s\n", la);
	printf("expected: \n%s\n", ti1);

	show_alloc_mem();

	printf("---------- FREE -----------------\n\n");

	ft_free(ti);
	printf("free ti\n");
	ft_free(la1);
	printf("free la1\n");
	ft_free(sa3);
	printf("free sa3\n");

	show_alloc_mem();

	return (0);
}
