/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cboyer <cboyer@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/11 11:52:17 by cboyer            #+#    #+#             */
/*   Updated: 2018/03/12 18:12:11 by cboyer           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void	*malloc_tiny(size_t size)
{
	t_plage	*plage;
	int		i;


	if (!zones.tiny)
	{
		zones.tiny = (void*)mmap(0, TINY_PLAGE + sizeof(t_plage), FLAG_PROT, FLAG_MAP, -1, 0);
		zones.tiny->next = 0x0;
		init_tab(zones.tiny->tab, 100, 0);
		zones.tiny->tab[0] = size;
		zones.tiny->type = 1;
		zones.tiny->ptr = zones.tiny + sizeof(t_plage);
		printf("%p\n", zones.tiny->ptr);
		return (zones.tiny->ptr);
	}
	else
	{
		plage = zones.tiny;
		while (plage)
		{
			i = 0;
			while (i < 100)
			{
				if (plage->tab[i] == 0)
				{
					plage->tab[i] = size;
					printf("%p\n", plage->ptr + i * TINY);
					return (plage->ptr + i * TINY);
				}
				i++;
			}
			plage = plage->next;
		}
	}
	create_plage(plage, 1);
	plage = plage->next;
	plage->tab[0] = size;
	plage->ptr = plage + sizeof(t_plage);
	printf("p: %p\n", plage->ptr);
	return (plage->ptr);
}

void	*malloc_small(size_t size)
{
	t_plage	*plage;
	int		i;


	if (!zones.small)
	{
		zones.small = (void*)mmap(0, SMALL_PLAGE + sizeof(t_plage), FLAG_PROT, FLAG_MAP, -1, 0);
		zones.small->next = 0x0;
		init_tab(zones.small->tab, 50, 0);
		zones.small->tab[0] = size;
		zones.small->type = 2;
		zones.small->ptr = zones.small + sizeof(t_plage);
		printf("%p\n", zones.small->ptr);
		return (zones.small->ptr);
	}
	else
	{
		plage = zones.small;
		while (plage)
		{
			i = 0;
			while (i < 50)
			{
				if (plage->tab[i] == 0)
				{
					plage->tab[i] = size;
					printf("%p\n", plage->ptr + i * SMALL);
					return (plage->ptr + i * SMALL);
				}
				i++;
			}
			if (plage->next == 0x0)
				break ;
			plage = plage->next;
		}
	}
	create_plage(plage, 2);
	plage = plage->next;
	plage->tab[0] = size;
	plage->ptr = plage + sizeof(t_plage);
	printf("%p\n", plage->ptr);
	return (plage->ptr);
}

void	*malloc_large(size_t size)
{
	void	*ptr;
	t_large	*large;

	if (!zones.large)
	{
		printf("taille: %lu\n", size);
		zones.large = (void*)mmap(0, size + sizeof(t_large),
							FLAG_PROT, FLAG_MAP, -1, 0);
		zones.large->next = 0x0;
		zones.large->size = size;
		zones.large->ptr = zones.large + sizeof(t_large);
		printf("large %p\n", zones.large);
		printf("ptr %p\n", zones.large->ptr);
		ptr = zones.large->ptr;
		zones.large->prev = 0x0;
	}
	else
	{
		large = zones.large;
		while (large->next)
			large = large->next;
		large->next = (void*)mmap(0, size + sizeof(t_large),
							FLAG_PROT, FLAG_MAP, -1 , 0);
		((t_large*)large->next)->prev = large;
		large = large->next;
		large->size = size;
		large->ptr = large + sizeof(t_large);
		ptr = large->ptr;
	}
	printf("%p\n", ptr);
	return (ptr);
}

void	*ft_malloc(size_t size)
{
	printf("Taille: %lu\n", size);
	if (!get_limit(size))
	{
		return (0x0);
	}
	if (size <= 0)
		return (0x0);
	else if (size < (size_t)TINY)
	{
		printf("TINY\n");
		return (malloc_tiny(size));
	}
	else if (size < (size_t)SMALL)
	{
		printf("SMALL\n");
		return (malloc_small(size));
	}
	else
	{
		printf("LARGE\n");
		return (malloc_large(size));
	}

}
