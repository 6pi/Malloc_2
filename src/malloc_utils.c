/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cboyer <cboyer@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/11 14:05:03 by cboyer            #+#    #+#             */
/*   Updated: 2018/03/12 15:21:35 by cboyer           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void	init_tab(int *tab, int size, int value)
{
	int	i;

	i = 0;
	while (i < size)
	{
		tab[i] = value;
		i++;
	}
}

void	create_plage(t_plage *plage, int type)
{
	t_plage	*new;

	new = mmap(0, type == 2 ? SMALL_PLAGE + sizeof(t_plage) : TINY_PLAGE + sizeof(t_plage), FLAG_PROT, FLAG_MAP, -1, 0);
	plage->next = new;
	new->prev = plage;
	new->ptr = new + sizeof(t_plage);
	new->type = type;
	init_tab(new->tab, type == 2 ? 50 : 100, 0);
	new->prev = plage;
}

int		get_limit(size_t size)
{
	struct rlimit           rlmt;
	unsigned long long      total;
	size_t                          sizes[3];

	sizes[0] = zones.tiny ? count_size(zones.tiny) : 0;
	sizes[1] = zones.small ? count_size(zones.small) : 0;
	sizes[2] = zones.large ? count_size_large(zones.large) : 0;
	total = (unsigned long long)(sizes[0] + sizes[1] + sizes[2] + size);
	if ((getrlimit(RLIMIT_AS, &rlmt) == -1) || total > rlmt.rlim_cur)
	{
		ft_putstr_fd("Not enough memorry to malloc", 2);
		return (0x0);
	}
	if (size > 1)
		return (1);
	else
		return 0;

}
