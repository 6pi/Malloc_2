/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cboyer <cboyer@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/16 12:50:07 by cboyer            #+#    #+#             */
/*   Updated: 2018/03/12 18:01:50 by cboyer           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void	*ft_realloc(void *ptr, size_t size)
{
	t_large	*large;
	t_plage	*plage;
	void	*new;
	int		i;
	printf("Realloc: %p, %lu\n", ptr, size);

	if (ptr == 0x0)
		return (ft_malloc(size));
	plage = find_plage(ptr);
	large = find_large(ptr);
	if (plage != 0x0 || large != 0x0)
	{
		printf("found\n");
		new = ft_malloc(size);
	}
	else
		return (0x0);
	if (large != 0x0)
	{
		ft_memcpy(new, large->ptr, size > (size_t)large->size ? (size_t)large->size : size);
		unmap_plage_large(large);
	}
	else if (plage != 0x0 && plage->type == 2)
	{
		i = ((int)ptr - (int)plage->ptr) / (int)SMALL;
		if (i > 50)
			return (0x0);
		ft_memcpy(new, plage->ptr, (size_t)plage->tab[i] > size ? size : (size_t)plage->tab[i]);
		plage->tab[i] = 0;
		check_del_plage(plage);
	}
	else if (plage != 0x0 && plage->type == 1)
	{
		i = ((int)ptr - (int)plage->ptr) / (int)TINY;
		if (i > 100)
			return (0x0);
		ft_memcpy(new, plage->ptr, (size_t)plage->tab[i] > size ? size : (size_t)plage->tab[i]);
		plage->tab[i] = 0;
		check_del_plage(plage);
	}
	else
		return (0x0);
	return (new);
}
