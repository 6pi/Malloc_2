/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc_mem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cboyer <cboyer@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/16 14:15:29 by cboyer            #+#    #+#             */
/*   Updated: 2018/03/12 18:03:12 by cboyer           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

int		print_tiny()
{
	t_plage	*plage;
	int		i;
	int	total;

	total = 0;
	plage = zones.tiny;
	ft_putstr("TINY : 0x");
	ft_itoa_base((int)zones.tiny, 16);
	ft_putstr("\n");
	while (plage)
	{
		i = 0;
		while (i < 100)
		{
			if (plage->tab[i] > 0)
			{
				ft_itoa_base((int)plage->ptr + (i * (int)TINY), 16);
				ft_putstr(" - ");
				ft_itoa_base((int)plage->ptr + (i * (int)TINY) + plage->tab[i], 16);
				ft_putstr(" : ");
				ft_putnbr(plage->tab[i]);
				ft_putstr(" octets\n");
				total += plage->tab[i];
			}
			i++;
		}
		plage = plage->next;
	}
	return (total);
}

int		print_small()
{
	t_plage	*plage;
	int		i;
	int		total;

	total = 0;
	plage = zones.small;
	ft_putstr("SMALL : 0x");
	ft_itoa_base((int)zones.small, 16);
	ft_putstr("\n");
	while (plage)
	{
		i = 0;
		while (i < 50)
		{
			if (plage->tab[i] > 0)
			{
				ft_itoa_base((int)plage->ptr + (i * (int)SMALL), 16);
				ft_putstr(" - ");
				ft_itoa_base((int)plage->ptr + (i * (int)SMALL) + plage->tab[i], 16);
				ft_putstr(" : ");
				ft_putnbr(plage->tab[i]);
				ft_putstr(" octets\n");
				total += plage->tab[i];
			}
			i++;
		}
		plage = plage->next;
	}
	return (total);
}

int		print_large()
{
	t_large	*large;
	int		total;

	total = 0;
	large = zones.large;
	ft_putstr("LARGE : 0x");
	ft_itoa_base((int)large, 16);
	ft_putstr("\n");
	while (large)
	{
		ft_itoa_base((int)large->ptr, 16);
		ft_putstr(" - ");
		ft_itoa_base((int)large->ptr + large->size, 16);
		ft_putstr(" : ");
		ft_putnbr(large->size);
		ft_putstr(" octets\n");
		total += large->size;
		large = large->next;
	}
	return (total);
}

void	show_alloc_mem()
{
	int	total;

	total = 0;
	if (zones.tiny)
		total += print_tiny();
	if (zones.small)
		total += print_small();
	if (zones.large)
		total += print_large();
	ft_putstr("TOTAL : ");
	ft_putnbr(total);
	ft_putstr("\n");
	return ;
}
